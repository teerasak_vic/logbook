# 2018-03-26

## ADEN 

[√] Meeting sanity 

    present about how other do sanity test

    we do post deployment by sanity 
    1. test without creating data for main scenario 
    2. test with pre/post script to clean data for new feature/bug fix/...
  
[√] FIX staging DB 

    ROUTE53
        * master-myl-fms => 10.229.193.10 
    EC2
        * aws-adn-th-stg-fms-rob-ubuntuX => 10.229.193.10 
        * aws-adn-th-stg-fms-myl-master  => 10.229.192.18 
        * aws-adn-th-stg-fms-myl-slave   => 10.229.195.6 

    problem from SD remove instance

[ ] IPT 
    
    ขอให้ช่วยตรวจสอบการคำนวนของ Order : 18021717176HG0E อีกครั้ง เนื่องจาก Shopee absorb ค่าใช้จ่ายบางส่วนให้ลูกค้า ซึ่ง IPT ต้องได้