# Vert.x 101

## WHAT 
* Event Loop ~ similar to Javascript, 100% non-blocking, reactive code through callback
* Verticles ~ encapsulate part of application, run completly independently of each other, communicate through an event bus
* toolkit, light weight, can use with other framework

## START 01

basic application with single verticle

structure:
* server verticle - configure the HTTP server and setup route 
* controller - handler method mapped route to service layer 
* services - contain business logic and "actual work"
* service launcher - configuring and deploying all verticles

```
$ mkdir vertx-gradle-101 && cd $
$ git init 
$ gradle init 
```

### Add gradle config 


