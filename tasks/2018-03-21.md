# 2018-03-21

## Delay

[√] OKRs for myself <2018-03-20>
[√] Update ADEN public API <2018-03-20>

## Tech 

[√] Research TiP (Test in Production)

    P.Aey need `Post Deployment Testing` not `TiP`
    it's difference story

## ADEN 

[√] {MEETING} with IPT 

    * invoice -- confirmation
    * report -- clear requirement
    
    Shopee's API 
    * shopee.order.GetEscrowDetails (ราคาส่วนลดต่างๆ)
    * shopee.order.GetOrderDetails (ข้อมูลของออเดอร์)

    Invoice 
    * ราคาต่อ line = ราคา - ส่วนลด
    * ค่าจัดส่ง
    * ส่วนลดเพิ่มเติม เช่น คูปอง

    Report 
    * เงินที่ลูกค้าจ่าย
    * เงินที่ IPT จะได้จาก Shopee

    === 
    Invoice 
    * ส่วนลด => ส่วนลดต่อชิ้น
    * ค่าจัดส่ง => เอาออก 
    * ส่วนลดเพิ่มเติม => เฉพาะที่ IPT ให้ลูกค้า เพื่อเอาไปคำนวน VAT
    * อาจจะมีกรณีที่ Shopee หัก IPT 

    ORDER: 18021717176HG0E มีปัญหาว่าเกิดราคา diff
    ราคาสุทธิ 270 ราคาลูกค้าจ่าย 290

    ===
    Order Report 
    > ส่ง order ทุกๆตัว ทั้งตัวที่ open / failed / shipped

    ===
    * automate report

    ===
    * ขึ้น merchant center เร็วสุด พฤษภา

    ===
    * bundling SKU 
    * freebie SKU

## Hackathon

* Connect young scientists and businesses
* Crowdfunding for Schools in Czech Republic
    Many czech schools lack funding to run interesting events for the kids or to renew the equipment faster, etc. Parents are not willing to spend huge amounts of money contributing directly and also do not have the full trust in the school management.
* Education Stocks *** บริษัท องค์กร กลุ่มทุน สามารถซื้อตัวนักเรียนนักศึกษาที่มีพรสวรรค์ ให้ทุน และสามารถซื้อขายกันได้ เหมือนนักกีฬาอาชีพ
    The idea is to make highly intelligent and gifted but underprivileged high school students into a tradable asset. Something similar to an IPO is issued and a certain amount of shares are issued that can be purchased as an investment or bought and sold for speculation. The money is then used directly to pay for a university. Any activity and awards achieved while in school will move the share value of the person. Upon completion of the college and getting a job, the person has the option of buying back their shares from the public completely or partially after a few years. This can also work for musicians, artists, or athletes.
* internship opportunities for teenagers
    I have tried googling up internship opportunities for teens lately, but the search results have not been successful. As a student enrolling in highschool I still have no idea what career I would like to pursue in the future! If i was given internship opportunities to explore and experiment with different fields it would greatly help with choosing my career. I was thinking of a website which a student would log in and search up internship opportunities present in their city. Based on their interest the website would direct the teen to a list of search results. The application would be listed for various internship opportunities and the employer would pick select candidates that would benifit most from the internship through their resume and cover letter.
* Resource Planning tool for Education Providers
* Connect schools and students






