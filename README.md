# ilumin's logbook
Where I keep note about my learnings in the wonderful and mysterious world

## Concept
from Jesper L. Andersen [On Logbooks](https://medium.com/@jlouis666/on-logbooks-e2380ab2f8f0#.2rox21s7w)

> The concept is extremely simple: you keep a log of your work, so you can refer back to it later. Yet, there are relatively few in Computer Science who does that, which is a sad state of affairs. The odd thing is that most computer scientists do know the importance of logging in an application. You see these elaborate logging frameworks that can log at different log levels, can forward logs between machines and so on. Yet�it looks like the CS people forgot that the same logging could apply to your own work.
