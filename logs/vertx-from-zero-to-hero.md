# Vertx from zero to (micro-)hero

## What's going on 

we are going to build application that's
* based on Vert.x (that’s why you are here, right?)
* distributed
* built as a reactive system

and we will learn 
* What Vert.x is and how to use its asynchronous non-blocking development model
* How to develop microservices with Vert.x with several types of services, and service discovery
* What verticles are and how to use them
* How to use the Vert.x event bus to send and receive messages
* How to expose HTTP endpoints with Vert.x, and also how to consume them
* How to compose asynchronous actions
* How to use several languages in the same application
* How to use databases with Vert.x
* How to manage failures with async results, futures, exception handlers and circuit breakers
* How to use Vert.x with RxJava

## Vert.x

a toolkit for building reactive applications on the JVM

## Application

financial app, where we will be making (virtual) money. The application is composed of a set of microservices:
* The quote generator - this is an absolutely unrealistic simulator that generates the quotes for 3 fictional companies MacroHard, Divinator, and Black Coat. The market data is published on the Vert.x event bus.
* The traders - these are a set of components that receives quotes from the quote generator and decides whether or not to buy or sell a particular share. To make this decision, they rely on another component called the portfolio service.
* The portfolio - this service manages the number of shares in our portfolio and their monetary value. It is exposed as a service proxy, i.e. an asynchronous RPC service on top of the Vert.x event bus. For every successful operation, it sends a message on the event bus describing the operation. It uses the quote generator to evaluate the current value of the portfolio.
* The audit - that’s the legal side, you know…​ We need to keep a list of all our operations (yes, that’s the law). The audit component receives operations from the portfolio service via an event bus and address . It then stores theses in a database. It also provides a REST endpoint to retrieve the latest set of operations.
* The dashboard - some UI to let us know when we become rich.

## REF
* (Vertx from zero to (micro-)hero)[http://escoffier.me/vertx-hol]