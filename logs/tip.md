# TiP ~ Test in Production

Testing in Production can be an extremely valuable tool in your QA arsenal, when used properly. Sure, there are always risks of testing with live users, but let’s face it – there are risks to NOT testing with live users as well. However, if you build the right procedures, TiP can result in a huge boost to your app’s overall quality.

## Data Driven Quality

put in place systems that will help your QA team receive and review operational data to measure quality. Make sure that testers have access to logs, performance metrics, alerts, and other information from the production environment, so they can be proactive in identifying and fixing problems.

> เหมือนกับที่เราดู log ของ order หลังจากที่ deploy

## Recovery Testing
## Fault Injection

to do something bad in system

## Synthetic User Testing

creation and monitoring of fake users which will interact with the real system

## A/B Split Testing

split user into n group to compare experience for each UI

## Controlled Test Flight

testing account and use that account to test and also control normal user to use old code

## Canary Testing

deploy code in small subset

===

Mission-critical
* Electricity 
* large company powering financial transactions
